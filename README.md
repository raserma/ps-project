# Provision and deploy a Go-Mongo cluster on OST, using Docker-swarm, Terraform and Consul.

## Technology Stack
* Openstack: to provide the infrastructure layer
* Terraform: to build and deploy the infrastructure
* Docker-swarm: to orchestrate containers
* Consul: to offer service discovery
* Go and MongoDB: as application layer
* Prometheus: to monitor the setup, bonus points.

## Architecture
To be defined 

### OpenStack
In order to deploy OST, I will use [DevStack](https://docs.openstack.org/devstack/latest/),
a series of scripts that bring a complete development Openstack environment.

The scripts take around 20 mins to build OST from scratch.